from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.authentication import Authentication
from tastypie.authorization import DjangoAuthorization, Authorization
from tastypie.exceptions import ImmediateHttpResponse
from tastypie.http import HttpForbidden, HttpBadRequest
from django.contrib.auth.models import User
from events.models import Event, Occurrence
from pytz.exceptions import UnknownTimeZoneError

import datetime

import logging
logger = logging.getLogger(__name__)


class UserResource(ModelResource):

    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        authentication = Authentication()
        authorization = DjangoAuthorization()


class OccurrenceResource(ModelResource):

    class Meta:
        queryset = Occurrence.objects.all()
        resource_name = 'occurrence_set'
        authentication = Authentication()
        authorization = Authorization()
        allowed_methods = ['get', 'post', 'put', 'delete']


class EventResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')
    occurrence_set = fields.ToManyField(OccurrenceResource, 'occurrence_set',
                                        full=True)

    class Meta:
        queryset = Event.objects.all()
        resource_name = 'event'
        authentication = Authentication()
        authorization = Authorization()
        allowed_methods = ['get', 'post', 'put', 'delete']
        detail_allowed_methods = ['get', 'post', 'put', 'delete']

    def get_list(self, request, **kwargs):
        '''Overiding to return get_occurrences'''
        try:
            timezone = request.user.get_profile().get_timezone_obj()
        except AttributeError:  # user is not logged in
            raise ImmediateHttpResponse(
                     response=HttpForbidden("login is required"))
        except UnknownTimeZoneError:
            raise ImmediateHttpResponse(
                    response=HttpBadRequest("Timezone not set"))

        start = datetime.datetime.today() \
                                  .replace(hour=0,
                                           minute=0,
                                           second=0,
                                           microsecond=0,
                                           tzinfo=timezone
                                  )
        end = start.replace(hour=23, minute=59, second=59)
        events = Event.objects.get_occurrences(start, end)

        # FIXME: this is a huge hack it should instead do the following:
        # - new custom Event manager that returns events during a period
        # - new Event method that returns all occurrences in an event period
        # - modify the bundle/hydrate to include the occurrences field
        clean_events = []
        for event, occurrences in events:
            occ = event.occurrence_set.all()[0]
            long_event = {
                            "description": event.description,
                            "id": event.pk,
                            "title": event.title,
                            "resource_uri": "/api/v1/event/%s/" % (event.pk),
                            "occurrences": occurrences,
                            "user": "/api/v1/user/1/",
                            "occurrence_set": {
                                "_rrule_params": occ._rrule_params,
                                "end_time": occ.end_time,
                                "id": occ.id,
                                "occurrence_end": occ.occurrence_end,
                                "resource_uri": "/api/v1/occurrence/%s/" % (occ.pk),
                                "rrule_frequency": occ.rrule_frequency,
                                "start_time": occ.start_time,
                            }
                        }
            clean_events.append(long_event)

        return self.create_response(request, clean_events)

    def obj_update(self, bundle, request=None, **kwargs):
        '''overiding updating of events and occurrences'''

        # update event
        event = Event.objects.get(pk=bundle.data['id'])
        event.title = bundle.data['title']
        event.description = bundle.data['description']
        event.save()

        # update occurrence
        occurrence = event.occurrence_set.all()[0]
        occ_data = bundle.data['occurrence_set']
        occurrence.end_time = occ_data['end_time']
        occurrence.start_time = occ_data['start_time']
        occurrence.rrule_frequency = occ_data['rrule_frequency']
        occurrence.save()

        return bundle

    def obj_create(self, bundle, request=None, **kwargs):
        '''overiding creating of events and occurrences'''
        logger.debug(bundle)

        # Create event
        event = Event.objects.create(
            title=bundle.data['title'],
            description=bundle.data['description'],
            user=request.user
        )

        # Create occurrence
        occ_data = bundle.data['occurrence_set']
        event.add_occurrences(
            start_time=occ_data['start_time'],
            end_time=occ_data['end_time'],
            rrule_frequency=occ_data['rrule_frequency'],
            rrule_params=occ_data['rrule_params']
        )

        return bundle
