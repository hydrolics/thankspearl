from events.models import Event
from events.models.occurrence import rrule_frequencies

from django.forms import ModelForm
from django import forms


class EventForm(ModelForm):
    start_time = forms.DateTimeField()
    end_time = forms.DateTimeField()
    rrule_frequency = forms.ChoiceField(choices=rrule_frequencies)

    class Meta:
        model = Event

    def save(self, commit=True, *args, **kwargs):
        model = super(EventForm, self).save(commit=False, *args, **kwargs)

        if commit:
            model.save()

            print self.cleaned_data['start_time'],
            print self.cleaned_data['end_time'],
            print self.cleaned_data['rrule_frequency']

            model.add_occurrences(self.cleaned_data['start_time'],
                                  self.cleaned_data['end_time'],
                                  self.cleaned_data['rrule_frequency'],
                                  ''
                                 )

        return model
