/*global $: false*/

var calendar = calendar || {};

$(function () {
    'use strict';
    new calendar.CalendarView();
});
