from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from utilities import create_event, create_occurrence
from events.models.occurrence import DateTimeAwareJSONEncoder
from events.models import Event
from datetime import datetime

import json


class EventResource(TestCase):

    def setUp(self):
        # create user
        self.username = 'test'
        self.password = 'test'
        self.user = User.objects.create_user(self.username,
                                             'test@example.com', self.password)

        self.user.get_profile().timezone = "US/Eastern"
        self.user.get_profile().save()

        # create event and occurrance
        self.event = create_event()
        self.occurrence = create_occurrence(event=self.event, rrule_params="")

        # urls
        self.obj_url = '/api/v1/event/%s/' % self.event.pk
        self.list_url = '/api/v1/event/'

        # Client
        self.client = Client()
        self.client.login(username=self.username, password=self.password)

    def test_get_list_unauthorized(self):
        ''' get_list should raise an error if the user isn't logged in '''
        self.client.logout()
        response = self.client.get(self.list_url, format='json',
                                   follow=True)
        self.assertEqual(response.status_code, 403)

    def test_get_list_no_timezone(self):
        ''' get_list should raise an error if the users timezone is not set '''
        self.user.get_profile().timezone = ""
        self.user.get_profile().save()

        response = self.client.get(self.list_url, format='json',
                                   follow=True)

        self.assertEqual(response.status_code, 400)

    def test_get_list_json(self):
        response = self.client.get(self.list_url, format='json')

        self.assertEqual(response.status_code, 200)

        # json should contain one event
        json_content = json.loads(response.content)
        self.assertEqual(len(json_content), 1)

    def test_get_obj(self):
        response = self.client.get(self.obj_url,
                                   format='json')

        self.assertEqual(response.status_code, 200)
        self.assertIn('title', response.content)
        self.assertIn('description', response.content)

    def test_update_obj(self):
        '''putting data should correctly save an event and occurrence'''

        new_event = {
            'description': 'new description',
            'title': 'new title'
        }

        new_occ = {
            'start_time': '2012-09-05T00:00:00+00:00',
            'end_time': '2012-09-06T00:00:00+00:00',
            'rrule_frequency': 'MONTHLY',
        }

        data = {
            "description": new_event['description'],
            "id": self.event.id,
            "title": new_event['title'],
            "resource_uri": "/api/v1/event/%s/" % (self.event.pk),
            "occurrences": "",
            "user": "/api/v1/user/1/",
            "occurrence_set": {
                "_rrule_params": self.occurrence._rrule_params,
                "end_time": new_occ['end_time'],
                "id": self.occurrence.id,
                "occurrence_end": self.occurrence.occurrence_end,
                "resource_uri": "/api/v1/occurrence/%s/" % (self.occurrence.pk),
                "rrule_frequency": new_occ['rrule_frequency'],
                "start_time": new_occ['start_time'],
            }
        }

        encoder = DateTimeAwareJSONEncoder()
        response = self.client.put(path=self.obj_url,
                                   content_type='application/json',
                                   data=encoder.encode(data))

        # Should return no data
        self.assertEqual(response.status_code, 204)

        # Event should be updated with the new data
        event = Event.objects.get(pk=self.event.id)
        for key, value in new_event.iteritems():
            self.assertEqual(value, getattr(event, key))

        # Occurrence should be updated with the new data
        occ = event.occurrence_set.all()[0]
        for key, value in new_occ.iteritems():
            # compare dates in their isoformat
            if type(getattr(occ, key)) == datetime:
                self.assertEqual(value, getattr(occ, key).isoformat())
            else:
                self.assertEqual(value, getattr(occ, key))
