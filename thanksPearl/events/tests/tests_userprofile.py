from django.test import TestCase
from django.contrib.auth.models import User

from datetime import datetime
from django.utils.timezone import utc
from pytz import timezone


class UserProfileTest(TestCase):

    def test_localize(self):
        '''localize should return a datetime in the users timezone'''
        user = User.objects.create()
        user.get_profile().timezone = 'US/Eastern'

        utc_datetime = datetime.now().replace(tzinfo=utc)
        localized_datetime = user.get_profile().localize(utc_datetime)

        # timezones are not the same
        self.assertNotEqual(localized_datetime.tzinfo, utc_datetime.tzinfo)

        # timezone is US/Eastern
        # checking type to account for SDT/DST
        self.assertEqual(type(localized_datetime.tzinfo),
                         type(timezone('US/Eastern'))
                        )
