/*global Backbone: false*/
/*global _: false*/
/*global $: false*/


var calendar = calendar || {};


(function () {
    'use strict';

    calendar.EventView = Backbone.View.extend({
        tagName: 'div',

        //el: '#calendar-day',

        events: {
            'click .event-delete': 'delete',
            'click .event-edit': 'edit',
        },

        initialize: function () {
            this.template = _.template($('#event-template').html());
            this.model.on('change', this.render, this);
        },

        render: function () {

            // Create all the occurrences
            _.each(this.model.get('occurrences'), function (occurrence) {
                var hour = new Date(occurrence).getHours();
                this._renderEvent(hour);
            }, this);

            return this;
        },


        /**
        * Finds the corresponding hour and creates an event at that location
        */
        _renderEvent: function (hour) {
            var position = $("#calendar-day-hour-" + hour).position();
            var topOffset = this._calculateEventOffset(this.model.get('occurrence_set').start_time);
            var width = $("#calendar-day-hour-" + hour).outerWidth();
            var height = this._calculateEventHeight(this.model.getDuration());

            var event = this.template(this.model.toJSON());

            // position the event over the hour
            event = $(event).css({
                'position': 'absolute',
                'top': position.top + topOffset,
                'left': position.left,
                'width': width,
                'height': height,
            });

            $(this.el).append(event);

        },


        /**
        * Calculates the height of an Event based upon it's duration.
        */
        _calculateEventHeight: function (durationMS) {
            var lineHeight = this._calculateLineHeight();
            var durationMin = durationMS / 1000 / 60;
            var minutePX = this._calculatePXforMin();

            var eventHeight = minutePX * durationMin;

            // the event shouldn't be smaller than the line height
            if (eventHeight < lineHeight) {
                eventHeight = lineHeight;
            }

            return eventHeight;

        },

        /**
        * Returns the offset of an Event's starting time in pixels
        */
        _calculateEventOffset: function (start_time) {
            var startMin = new Date(start_time).getMinutes();
            return startMin * this._calculatePXforMin();
        },

        /**
        * Returns the line-height of a calendar Event.
        */
        _calculateLineHeight: function () {
            var lineHeight = $('#calendar-day-hour-0').css('line-height')
                                                      .replace('px', '');

            return lineHeight;
        },

        /**
        * Returns the number of pixels each minute a calendar event is.
        */
        _calculatePXforMin: function () {
            // grab first calendars elements height, which is a half hour.
            var halfHourPX = $("#calendar-day-hour-0").outerHeight();
            var hourPX = halfHourPX * 2;
            var minutePX = hourPX / 60;

            return minutePX;
        },

        delete: function () {
            this.$el.remove();
            this.model.destroy();
        },

        edit: function () {
            var $edit = $('#event-edit');
            //$edit.find('#edit-title').val(this.model.get('title'));
            //$edit.find('#edit-description').val(this.model.get('description'));

            new calendar.EventEditView({model: this.model});
        },



    });


    calendar.EventEditView = Backbone.View.extend({

        tagName: 'div',

        el: '#event-edit',

        events: {
            'click #edit-save': 'eventSave',
        },

        initialize: function () {
            this.template = _.template($('#event-edit-template').html());

            this.model.on('error', this.errorHandler, this);

            this.render();
        },

        render: function () {
            this.$el.html(this.template(this.model.toJSONWithHelpers()));
            return this;
        },

        eventSave: function () {
            var occurrence = this.model.get('occurrence_set');
            var startPTime = this.$el.find('#edit-start_time').val();
            var endPTime = this.$el.find('#edit-end_time').val();

            this.model.save({
                title: this.$el.find('#edit-title').val(),

                /* Have to save the full occurrence_set since it's nested */
                occurrence_set: {
                    start_time: this._partialTimeToFull(startPTime, occurrence.start_time),
                    end_time: this._partialTimeToFull(endPTime, occurrence.end_time),
                    rrule_frequency: occurrence.rrule_frequency,
                    rrule_params: occurrence.rrule_params,
                    occurrence_end: occurrence.occurrence_end,
                }
            });
        },

        /**
        * converts a partial time string to a full datetime
        */
        _partialTimeToFull: function (partialTime, dateString) {
            var partialSplit = partialTime.split(':');
            var date = new Date(dateString);

            date.setHours(partialSplit[0]);
            date.setMinutes(partialSplit[1]);

            return date.toISOString();
            
        },

        errorHandler: function (model, error) {
            console.log(model, error);
        }

    });




}());
