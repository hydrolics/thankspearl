from django.db import models

from events.models import Event

from django.utils.timezone import utc
from datetime import datetime, timedelta
from dateutil import rrule
import dateutil.rrule
from json import JSONEncoder, JSONDecoder


rrule_frequencies = (
    ('YEARLY', 'Yearly'),
    ('MONTHLY', 'Monthly'),
    ('WEEKLY', 'Weekly'),
    ('DAILY', 'Daily'),
    ('HOURLY', 'Hourly'),
    ('MINUTELY', 'Minutely'),
    ('SECONDLY', 'Secondly'),
)


# http://taketwoprogramming.blogspot.com/2009/06/subclassing-jsonencoder-and-jsondecoder.html

class DateTimeAwareJSONEncoder(JSONEncoder):
    """Converts a python object, where datetime and timedelta objects are
    converted into objects that can be decoded using the
    DateTimeAwareJSONDecoder.
    """
    def default(self, obj):
        if isinstance(obj, datetime):
            return {
                '__type__': 'datetime',
                'year': obj.year,
                'month': obj.month,
                'day': obj.day,
                'hour': obj.hour,
                'minute': obj.minute,
                'second': obj.second,
                'microsecond': obj.microsecond,
            }

        elif isinstance(obj, timedelta):
            return {
                '__type__': 'timedelta',
                'days': obj.days,
                'seconds': obj.seconds,
                'microseconds': obj.microseconds,
            }

        else:
            return JSONEncoder.default(self, obj)


class DateTimeAwareJSONDecoder(JSONDecoder):
    """Converts a json string, where datetime and timedelta objects were
    converted into objects using the DateTimeAwareJSONEncoder, back into a
    python object.
    """

    def __init__(self):
            JSONDecoder.__init__(self, object_hook=self.dict_to_object)

    def dict_to_object(self, d):
        if '__type__' not in d:
            return d

        type = d.pop('__type__')
        if type == 'datetime':
            return datetime(**d).replace(tzinfo=utc)
        elif type == 'timedelta':
            return timedelta(**d)
        else:
            # Oops... better put this back together.
            d['__type__'] = type
            return d


def datetime_handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError, 'Object of type %s with value of %s is not JSON serializable' % (type(obj), repr(obj))


class Occurrence(models.Model):
    '''Represents the start end time for a specific occurrence of a master
    ``Event`` object.
    '''
    start_time = models.DateTimeField('start time')
    end_time = models.DateTimeField('end time')
    occurrence_end = models.DateTimeField('Occurrence End', null=True,
                                          blank=True
                                         )
    event = models.ForeignKey(Event, verbose_name='event')

    # rrule
    rrule_frequency = models.CharField('rrule_frequency',
                                       choices=rrule_frequencies,
                                       max_length=10
                                      )
    _rrule_params = models.TextField('rrule_params', blank=True,
                                     editable=False
                                    )

    class Meta:
        app_label = 'events'

    def __unicode__(self):
        return u'%s: %s' % (self.title, self.start_time.isoformat())

    def __cmp__(self, other):
        return cmp(self.start_time, other.start_time)

    def save(self, *args, **kwargs):
        self.set_occurrence_end()
        super(Occurrence, self).save(*args, **kwargs)

    @property
    def title(self):
        return self.event.title

    @property
    def rrule_params(self):
        ''' deserializes rrule params '''

        if not self._rrule_params:
            return None

        decoder = DateTimeAwareJSONDecoder()

        return decoder.decode(self._rrule_params)

    @rrule_params.setter
    def rrule_params(self, params):
        ''' Serializes rrule params to insert into database '''

        encoder = DateTimeAwareJSONEncoder()
        self._rrule_params = encoder.encode(params)

        self.save()

    def set_occurrence_end(self):
        ''' determines if there is an occurrence end and sets it '''

        params = self.rrule_params
        if params:
            frequency = getattr(dateutil.rrule, self.rrule_frequency)

            if params.get('until'):
                # set to the until date
                self.occurrence_end = self.rrule_params['until']

            elif params.get('count'):
                # set to the last date in count
                params['dtstart'] = self.start_time
                self.occurrence_end = list(rrule.rrule(frequency,
                                                       **params))[-1]
