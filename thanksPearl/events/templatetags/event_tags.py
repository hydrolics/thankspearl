from django import template

register = template.Library()


@register.filter(name='hour')
def hour(number):
    '''converts a number to an hour'''
    if number >= 12:
        period = "pm"
    else:
        period = "am"

    if number in (12, 0):
        return "12:00 %s" % (period)
    else:
        return "%s:00 %s" % (number % 12, period)
