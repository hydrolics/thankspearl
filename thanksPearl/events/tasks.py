from celery import task

from events.models import Event

from datetime import datetime
from django.utils.timezone import utc

from urllib import urlencode
from urllib2 import urlopen, URLError


@task()
def add(x, y):
    return x + y


@task()
def get_events_this_minute():
    start = datetime.utcnow().replace(second=0, microsecond=0, tzinfo=utc)
    end = start.replace(second=59)
    events = Event.objects.get_occurrences(start, end)
    print '%s Events: %s' % (start, events)

    for items in events:
        event, occurrences = items
        dispatch_event.delay(event, event.user)


@task()
def dispatch_event(event, user):
    url = 'http://api.tropo.com/1.0/sessions?action=create&token=41da7856b830ee41a42a47b24db3e462a64f359699c43187b177967bc179a0610a4a2709ac27ac645600693e&'
    msg = urlencode({
        'msg': event.title
    })
    tropo_uri = '%s&%s' % (url, msg)

    urlopen(tropo_uri)
