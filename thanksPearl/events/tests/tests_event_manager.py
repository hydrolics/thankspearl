from django.test import TestCase
from django.contrib.auth.models import User

from events.models import Event

import datetime
from django.utils.timezone import utc


class EventManagerTest(TestCase):

    event_create = {
        'title':  'test',
        'description':  'test',
        'user':  User.objects.get(pk=1),
    }

    def test_get_occurrence_range(self):
        ''' get_occurrence should work for large ranges '''

        event = Event.objects.create(**self.event_create)
        event.add_occurrences(
            start_time=datetime.datetime(2001, 1, 1, 0).replace(tzinfo=utc),
            end_time=datetime.datetime(2091, 1, 1, 0).replace(tzinfo=utc),
            rrule_frequency='DAILY',
            rrule_params=''
        )

        start = datetime.datetime.today() \
                                  .replace(hour=0,
                                           minute=0,
                                           second=0,
                                           microsecond=0,
                                           tzinfo=utc
                                  )
        end = start.replace(hour=23, minute=59, second=59)

        events = Event.objects.get_occurrences(start, end)

        self.assertEqual(len(events), 1)

    def test_get_occurrence_today(self):
        ''' get_occurrence should show an event created for today '''

        event = Event.objects.create(**self.event_create)

        start_time = datetime.datetime.now().replace(tzinfo=utc)
        end_time = start_time + datetime.timedelta(minutes=30)
        event.add_occurrences(
            start_time=start_time,
            end_time=end_time,
            rrule_frequency='DAILY',
            rrule_params=''
        )

        #print Occurrence.objects.get(pk=1).start_time
        #print event.occurrences(start, end)
        start = datetime.datetime.today() \
                                  .replace(hour=0,
                                           minute=0,
                                           second=0,
                                           microsecond=0,
                                           tzinfo=utc
                                  )
        end = start.replace(hour=23, minute=59, second=59)

        events = Event.objects.get_occurrences(start, end)

        self.assertEqual(len(events), 1)

    def test_get_occurrence_count(self):
        ''' get occurrence should return correct number of events '''

        start_time = datetime.datetime(2012, 1, 1, 01, 00).replace(tzinfo=utc)
        end_time = datetime.datetime(2012, 1, 1, 01, 02).replace(tzinfo=utc)

        # Should not show up in the list
        event1 = Event.objects.create(**self.event_create)
        event1.title = 'Daily'
        event1.save()
        event1.add_occurrences(
            start_time=start_time,
            end_time=end_time,
            rrule_frequency='DAILY',
            rrule_params=''
        )

        # Should show up in the list
        event2 = Event.objects.create(**self.event_create)
        event2.title = 'Minutely'
        event2.save()
        event2.add_occurrences(
            start_time=start_time,
            end_time=end_time,
            rrule_frequency='MINUTELY',
            rrule_params=''
        )

        start = datetime.datetime.today() \
                                 .replace(second=0,
                                          microsecond=0,
                                          tzinfo=utc
                                 )
        end = start.replace(second=59)

        events = Event.objects.get_occurrences(start, end)
        self.assertEqual(len(events), 1)
