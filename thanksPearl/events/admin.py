from events.models import Event, Occurrence, UserProfile
from django.contrib.auth.models import User

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin


class OccurrenceAdmin(admin.ModelAdmin):
    readonly_fields = ('_rrule_params',)


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'profile'


class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )

admin.site.register(Event)
admin.site.register(Occurrence, OccurrenceAdmin)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
