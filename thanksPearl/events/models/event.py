from django.db import models
from django.contrib.auth.models import User

#from events.models import Occurrence

from datetime import datetime
from dateutil.rrule import rrule
import dateutil.rrule
from django.utils.timezone import utc, make_naive
from django.db.models import Q


class EventManager(models.Manager):
    use_for_related_fields = True

    def get_occurrences(self, start, end):
        ''' returns a queryset of Events that fall within start and end '''

        events = super(EventManager, self) \
                  .get_query_set() \
                  .filter(
                          # Starting before the time slice
                          Q(
                            occurrence__start_time__lte=start
                          ) |

                          # Or starting during the time slice
                          Q(
                            occurrence__start_time__gte=start,
                            occurrence__start_time__lte=end
                          ) |

                          # Or ending during the time slice
                          Q(
                            occurrence__occurrence_end__gte=start,
                            occurrence__occurrence_end__lte=end,
                          )
                  )

        clean_events = []
        for event in events:
            clean_occurrences = [occ for occ in event.occurrences(start, end)
                                 if occ]

            if clean_occurrences:
                clean_events.append((event, clean_occurrences))

        return clean_events

    def get_today(self):
        ''' convenience method for calling get_occurrences with today '''
        start = datetime.now() \
                        .replace(tzinfo=utc) \
                        .replace(hour=0,
                                 minute=0,
                                 second=0,
                                 microsecond=0
                        )
        end = start.replace(hour=23, minute=59, second=59)

        return self.get_occurrences(start, end)


class Event(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField('title', max_length=32)
    description = models.CharField('description', max_length=255)

    objects = EventManager()

    class Meta:
        app_label = 'events'

    def __unicode__(self):
        return self.title

    def add_occurrences(self, start_time, end_time,
                        rrule_frequency, rrule_params):
        ''' Creates associate occurrence models '''

        occurrence = self.occurrence_set.create(
                                            start_time=start_time,
                                            end_time=end_time
                                         )
        occurrence.rrule_frequency = rrule_frequency
        occurrence.rrule_params = rrule_params

        occurrence.save()

    def occurrences(self, start, end):
        ''' returns a list of occurrences for the instance of Event '''

        occurrence = self.occurrence_set.all()[0]
        frequency = getattr(dateutil.rrule, occurrence.rrule_frequency)
        params = occurrence.rrule_params or {}

        if start.tzinfo != end.tzinfo:
            raise TypeError('Unable to support mixed timezones in start and end')

        timezone = start.tzinfo

        params['dtstart'] = occurrence.start_time

        if occurrence.occurrence_end:
            params['until'] = occurrence.occurrence_end
        else:
            params['until'] = end

        # Passing a timezone aware datetime to rrule causes it to raise a type
        # error.  To get past this we remove all the timezone information
        # and replace it in the returned results
        for key, value in params.iteritems():
            try:
                params[key] = make_naive(value, timezone)
            # Ignore keys that don't have datetimes
            except AttributeError:
                pass

        start = make_naive(start, timezone)
        end = make_naive(end, timezone)

        rule = rrule(frequency, **params)
        rules = rule.between(start, end, inc=True)

        # make rrule timezone aware
        tz_aware_rules = [rule.replace(tzinfo=timezone)
                             for rule in list(rules)]

        return tz_aware_rules

    def today(self):
        ''' convenience method for calling occurrences with today '''
        start = datetime.now() \
                        .replace(tzinfo=utc) \
                        .replace(hour=0,
                                 minute=0,
                                 second=0,
                                 microsecond=0
                        )
        end = start.replace(hour=23, minute=59, second=59)

        return self.occurrences(start, end)
