from django.conf.urls import patterns, include, url
from events.api import EventResource, OccurrenceResource, UserResource

import datetime
from tastypie.api import Api

from django.contrib import admin
admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(EventResource())
v1_api.register(OccurrenceResource())
v1_api.register(UserResource())

urlpatterns = patterns('',
    # Admin
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # Events
    url(r'events/today/$', 'events.views.day',
        {
            'year': datetime.date.today().year,
            'month': datetime.date.today().month,
            'day': datetime.date.today().day
        }),

    url(r'event/create/$', 'events.views.event_create', name='event-create'),

    url(r'^events/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/$',
        'events.views.day'),

    url(r'^calendar/$', 'events.views.calendar'),
    url(r'^calendar/test/$', 'events.views.test'),

    # Tropo
    url(r'tropo/dispatch/$', 'dispatch.views.tropo_index'),

    url(r'^api/', include(v1_api.urls)),
)
