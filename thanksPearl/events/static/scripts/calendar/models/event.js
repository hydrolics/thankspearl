/*global Backbone: false*/
/*global _: false*/


var calendar = calendar || {};

(function () {
    'use strict';

    calendar.Event = Backbone.Model.extend({

        defaults: {
            title: '',
            description: '',
        },
        
        initialize: function () {

        },

        getDuration: function () {
            var occurrence = this.get('occurrence_set');
            var start = new Date(occurrence.start_time).getTime();
            var end = new Date(occurrence.end_time).getTime();

            return end - start;
        },

        getStartFullDate: function () {
            var occurrence = this.get('occurrence_set');
            return this._toFullDate(occurrence.start_time);
        },

        getStartPartialTime: function () {
            var occurrence = this.get('occurrence_set');
            return this._toPartialTime(occurrence.start_time);
        },

        getEndFullDate: function () {
            var occurrence = this.get('occurrence_set');
            return this._toFullDate(occurrence.end_time);
        },

        getEndPartialTime: function () {
            var occurrence = this.get('occurrence_set');
            return this._toPartialTime(occurrence.end_time);
        },

        /**
        * Adds helper methods to the toJSON results
        */
        toJSONWithHelpers: function () {
            var json = this.toJSON();
            return _.extend(json, {
                startPartialTime: this.getStartPartialTime(),
                endPartialTime: this.getEndPartialTime()
            });

        },

        /**
        * Returns the full date of an event: 2012-12-27
        */
        _toFullDate: function (dateString) {
            var date = new Date(dateString);
            return date.getFullYear() + '-'
                + date.getMonth() + '-'
                + date.getDay();
        },

        /**
        * Returns the partial time of an event: 23:47:57
        */
        _toPartialTime: function (dateString) {
            var date = new Date(dateString);
            return date.toLocaleTimeString();
        },



    });


}());
