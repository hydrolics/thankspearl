/*global Backbone: false*/
/*global _: false*/
/*global $: false*/


var calendar = calendar || {};


(function () {
    'use strict';

    calendar.CalendarView = Backbone.View.extend({
        el: '#calendar',

        initialize: function () {
            calendar.Events.on('add', this.addOne, this);
            calendar.Events.on('reset', this.addAll, this);
            calendar.Events.fetch({error: function () { console.log(arguments); }});
            var day = new calendar.DayView();
        },

        render: function () {

        },

        addOne: function (event) {
            var view = new calendar.EventView({model: event});
            $('#calendar').append(view.render().el);
        },

        addAll: function () {
            calendar.Events.each(this.addOne, this);
        }


    });


}());
