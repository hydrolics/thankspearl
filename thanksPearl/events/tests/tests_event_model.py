from django.test import TestCase
from django.contrib.auth.models import User

from events.models import Event

from utilities import create_event, create_occurrence
import datetime
from django.utils.timezone import utc, is_aware
from pytz import timezone


class EventModelTest(TestCase):

    event_create = {
        'title':  'test',
        'description':  'test',
        'user':  User.objects.get(pk=1),
    }

    occurrence_create = {
        'start_time':  datetime.datetime(2012, 9, 3, 0).replace(tzinfo=utc),
        'end_time':  datetime.datetime(2012, 9, 4, 0).replace(tzinfo=utc),
        'rrule_frequency':  'HOURLY',
        'rrule_params':  {'count': 3},
    }

    def test_add_occurrence(self):
        ''' add_occurrences should create a new occurrence object and add the
        correct data '''

        event = Event.objects.create(**self.event_create)
        event.add_occurrences(**self.occurrence_create)
        occurrence = event.occurrence_set.all()[0]

        # Basic setting of the fields
        self.assertEqual(
            occurrence.start_time,
            self.occurrence_create['start_time']
        )
        self.assertEqual(
            occurrence.end_time,
            self.occurrence_create['end_time']
        )
        self.assertEqual(
            occurrence.rrule_frequency,
            self.occurrence_create['rrule_frequency']
        )
        self.assertEqual(
            occurrence.rrule_params,
            self.occurrence_create['rrule_params']
        )

    def test_occurrences(self):
        ''' occurrences should return a list with all the occurrences '''

        event = Event.objects.create(**self.event_create)
        event.add_occurrences(**self.occurrence_create)
        occurrences = event.occurrence_set.all()[0]
        occurrences.rrule_params = self.occurrence_create['rrule_params']

        start = datetime.datetime(2012, 9, 1, 0).replace(tzinfo=utc)
        end = datetime.datetime(2012, 9, 7, 0).replace(tzinfo=utc)

        occurrence_dates = event.occurrences(start, end)

        # returns a list
        self.assertEqual(type(occurrence_dates), type([]))

        # list is the correct length
        self.assertEqual(len(occurrence_dates),
                         self.occurrence_create['rrule_params']['count'])

        for occ in occurrence_dates:
            # all elements in the list are datetime objects
            self.assertEqual(type(occ), type(datetime.datetime.now()))

            # all elements are timezone aware
            self.assertEqual(is_aware(occ), True)

            # all elements start after the start_time
            self.assertGreaterEqual(occ, occurrences.start_time)

            # all elements end before the end_time
            self.assertLessEqual(occ, occurrences.end_time)

    def test_occurrences_range(self):
        '''occurrences should only be within the start_time and
        end_time range
        '''

        event = Event.objects.create(**self.event_create)
        event.add_occurrences(**self.occurrence_create)
        occurrences = event.occurrence_set.all()[0]

        before = (self.occurrence_create['start_time'] -
                    datetime.timedelta(weeks=1))
        before.replace(tzinfo=utc)
        after = (self.occurrence_create['end_time'] +
                    datetime.timedelta(weeks=1))
        after.replace(tzinfo=utc)

        occurrence_dates = event.occurrences(before, after)

        for occ in occurrence_dates:
            # all elements start after the start_time
            self.assertGreaterEqual(occ, occurrences.start_time)

            # all elements end before the end_time
            self.assertLessEqual(occ, occurrences.end_time)

    def test_occurrences_none(self):
        '''occurrences should return an empty list if there are no
        occurrences in the range
        '''

        event = Event.objects.create(**self.event_create)
        event.add_occurrences(**self.occurrence_create)

        before = (self.occurrence_create['start_time'] +
                    datetime.timedelta(weeks=32))
        before.replace(tzinfo=utc)
        after = (self.occurrence_create['end_time'] +
                    datetime.timedelta(weeks=33))
        after.replace(tzinfo=utc)

        occurrence_dates = event.occurrences(before, after)

        self.assertEqual(len(occurrence_dates), 0)

    def test_occurrences_count(self):
        '''occurrences should return the correct number of occurrences'''
        event = Event.objects.create(**self.event_create)
        event.add_occurrences(
            start_time=self.occurrence_create['start_time'],
            end_time=self.occurrence_create['end_time'],
            rrule_frequency='MINUTELY',
            rrule_params={}
        )

        events = event.today()
        self.assertEqual(len(events), 1440)

        event2 = Event.objects.create(**self.event_create)
        event2.add_occurrences(
            start_time=datetime.datetime(2012, 10, 2, 23, 18, 11, tzinfo=utc),
            end_time=datetime.datetime(2012, 10, 2, 23, 18, tzinfo=utc),
            rrule_frequency='MINUTELY',
            rrule_params={}
        )

        events = event2.today()
        self.assertEqual(len(events), 1440)

    def test_occurrence_time(self):
        '''Occurrences should return an occurrence with the correct time'''
        start_time = datetime.datetime(2009, 1, 1, 9, 45).replace(tzinfo=utc)
        end_time = datetime.datetime(2009, 1, 1, 10, 00).replace(tzinfo=utc)

        event = Event.objects.create(**self.event_create)
        event.add_occurrences(
            start_time=start_time,
            end_time=end_time,
            rrule_frequency='DAILY',
            rrule_params={}
        )

        events = event.today()

        self.assertEqual(events[0].hour, start_time.hour)
        self.assertEqual(events[0].minute, start_time.minute)
        self.assertEqual(events[0].second, start_time.second)

        #self.assertEqual(events[0].hour, end_time.hour)
        #self.assertEqual(events[0].minute, end_time.minute)
        #self.assertEqual(events[0].second, end_time.second)

    def test_occurrence_off_day(self):
        '''Occurrences shouldn't return on days it's not supposed to'''
        start_time = datetime.datetime(2009, 1, 1, 9, 45).replace(tzinfo=utc)
        end_time = datetime.datetime(2009, 1, 1, 10, 00).replace(tzinfo=utc)

        # Event occurres every other day
        event = Event.objects.create(**self.event_create)
        event.add_occurrences(
            start_time=start_time,
            end_time=end_time,
            rrule_frequency='DAILY',
            rrule_params={'interval': 2}
        )

        off_start = start_time.replace(day=2, hour=0, minute=0)
        off_end = end_time.replace(day=2, hour=23, minute=59)
        events = event.occurrences(off_start, off_end)

        self.assertEqual(len(events), 0)

    def test_occurrence_timezone(self):
        '''occurrence should return occurrences in the same timezone'''
        event = create_event()
        create_occurrence(event)
        eastern = timezone('US/Eastern')
        start_time = eastern.localize(datetime.datetime(2012, 9, 2, 0))
        end_time = eastern.localize(datetime.datetime(2012, 9, 4, 0))

        occs = event.occurrences(start_time, end_time)

        for occ in occs:
            self.assertEqual(type(occ.tzinfo), type(eastern))

    def test_occurrence_timezone_mixed(self):
        '''mixed timezones should raise an error'''
        event = create_event()
        create_occurrence(event)
        eastern = timezone('US/Eastern')
        start_time = eastern.localize(datetime.datetime(2012, 9, 2, 0))
        end_time = datetime.datetime(2012, 9, 4, 0).replace(tzinfo=utc)

        self.assertRaises(TypeError, event.occurrences, (start_time, end_time))

    def test_today(self):
        ''' today should return occurrences for today only '''

        start = datetime.datetime.now().replace(tzinfo=utc)
        end = start + datetime.timedelta(weeks=1)

        event = Event.objects.create(**self.event_create)
        event.add_occurrences(
            start_time=start,
            end_time=end,
            rrule_frequency='DAILY',
            rrule_params={}
        )

        today = event.today()

        self.assertEqual(len(today), 1)
