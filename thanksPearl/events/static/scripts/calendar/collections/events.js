/*global Backbone: false*/


var calendar = calendar || {};


(function () {
    'use strict';

    var EventList = Backbone.Collection.extend({
        model: calendar.Event,
        url: '/api/v1/event/',

        initialize: function () {

        }

    });

    calendar.Events = new EventList();

}());
