from django.contrib.auth.models import User

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from pytz import common_timezones, timezone


class UserProfile(models.Model):
    '''Additional `User` information'''
    user = models.OneToOneField(User)

    timezone = models.CharField(max_length=50, blank=True)

    class Meta:
        app_label = 'events'

    def __unicode__(self):
        return u'%s: %s' % (self.user.get_full_name, self.timezone)

    def clean(self):
        if self.timezone not in common_timezones:
            raise ValidationError(_('Invalid timezone'))

    def localize(self, non_localized_datetime):
        '''Returns a datetime converted to the users timezone'''
        return non_localized_datetime.astimezone(timezone(self.timezone))

    def get_timezone_obj(self):
        '''Returns a pytz timezone object in the users timezone'''
        return timezone(self.timezone)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    '''Create a `UserProfile` if one doesn't exist'''
    if created:
        UserProfile.objects.create(user=instance)
