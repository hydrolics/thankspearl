from tropo import Tropo, Session
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def tropo_index(request):
    '''initiates requests with tropo service'''
    if request.POST:
        # parse passed in parameters
        session = Session(request.body)

        t = Tropo()
        t.call('17174138746')

        # http://www.freesound.org/data/previews/51/51710_113976-lq.mp3
        t.say('http://audiomicro-dev.s3.amazonaws.com/preview/20017/571436cb23f5955')

        t.say("Hi, it's Pearl!")
        t.say("time to start your next task")
        t.say(session.parameters['msg'])

    return HttpResponse(t.RenderJson(), mimetype='application/json')
