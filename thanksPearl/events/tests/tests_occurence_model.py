from django.test import TestCase
from django.utils.timezone import utc

import datetime
from utilities import create_event, create_occurrence


class OccurenceModelTest(TestCase):

    def test_set_rrule_param(self):
        '''rrule_param should serialize the params and put them in
        the database
        '''
        event = create_event()
        params = {'count': 3}
        valid_json_string = '{"count": 3}'
        occurrence = create_occurrence(event, rrule_params=params)

        self.assertEqual(occurrence._rrule_params, valid_json_string)

    def test_get_rrule_param(self):
        '''rrule_param should return a python object identical
        to the params
        '''

        event = create_event()
        params = {'count': 3}
        occurrence = create_occurrence(event, rrule_params=params)

        self.assertEqual(occurrence.rrule_params, params)

    def test_get_rrule_param_none(self):
        ''' rrule_param should return None if nothing is set '''

        event = create_event()
        params = None
        occurrence = create_occurrence(event, rrule_params=params)

        #occurrence = Occurrence.objects.get(pk=1)
        self.assertEqual(occurrence.rrule_params, None)

    def test_set_occurrence_end_until(self):
        '''if there is an until in the rrule params set the
        occurrence_end to until
        '''
        until = datetime.datetime(2014, 9, 11).replace(tzinfo=utc)
        event = create_event()
        occurrence = create_occurrence(event, rrule_params={'until': until})

        self.assertEqual(occurrence.occurrence_end, until)

    def test_set_occurrence_end_count(self):
        '''if there is a count in the rrule params set the occurrence_end
        to the last one
        '''
        event = create_event()
        count = 7
        params = {'count': count}
        occurrence = create_occurrence(event, rrule_params=params,
                                       rrule_frequency='DAILY')

        # minus one day since we count the first one
        last = occurrence.start_time + datetime.timedelta(days=count - 1)

        self.assertEqual(occurrence.occurrence_end, last)
