/*global Backbone: false*/
/*global _: false*/
/*global $: false*/


var calendar = calendar || {};


(function () {
    'use strict';

    calendar.DayView = Backbone.View.extend({
        el: '#calendar-day-container',

        events: {
            'click #calendar-day td': 'create',
        },

        initialize: function () {
            console.log(this.el);

            this.date = new Date();
            this.date.setHours(0);
            this.date.setMinutes(0);
            this.date.setSeconds(0);
        },

        render: function () {

        },

        create: function (event) {
            var id = event.currentTarget.id;
            var hour = this._hourFromId(id);
            var minute = this._minuteFromId(id);
            console.log(minute);

            var start_time = new Date(this.date.getTime());
            start_time.setHours(hour);
            start_time.setMinutes(minute);

            var end_time = new Date(start_time.getTime());
            end_time.setTime(start_time.getTime() + (60 * 60 * 1000));

            var model = calendar.Events.create({
                title: 'test',
                description: 'test',
                occurrences: [start_time.toISOString()],
                occurrence_set: {
                    start_time: start_time.toISOString(),
                    end_time: end_time.toISOString(),
                    rrule_params: '',
                    rrule_frequency: 'DAILY'
                }
            });

            new calendar.EventEditView({model: model});

        },

        // Returns an hour from the id of a calendar element
        _hourFromId: function (id) {
            var time = _.last(id.split('-'));
            return parseInt(time, 10);
        },

        // Returns minutes from the id of a calendar element
        _minuteFromId: function (id) {
            var time = _.last(id.split('-'));
            var minutesFrac = time.split('.')[1] * 0.1;

            var minutes = _.isNaN(minutesFrac) ? 0: 60 * minutesFrac;
            return minutes;
        },

    });


}());
