from events.models import Event
from events.forms import EventForm

from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext
from django.utils import simplejson

from datetime import datetime
from pytz import timezone


def day(request, year, month, day):
    ''' Displays all events for a given day '''

    year = int(year)
    month = int(month)
    day = int(day)
    user_timezone = timezone(request.user.get_profile().timezone)

    start = user_timezone.localize(datetime(year, month, day, 0))
    end = user_timezone.localize(datetime(year, month, day, 23, 59, 59))

    events = Event.objects.get_occurrences(start, end)

    return render_to_response('events/day.html',
                              {'events': events},
                              context_instance=RequestContext(request)
                             )


def event_create(request):
    if request.method == 'POST':
        eventform = EventForm(request.POST)
        if eventform.is_valid():
            eventform.save()
    else:
        eventform = EventForm()

    print eventform
    return render_to_response('events/event-create.html',
                              {'eventform': eventform},
                              context_instance=RequestContext(request)
                             )


def calendar(request):
    return render_to_response('events/calendar.html', {},
                              context_instance=RequestContext(request))


def test(request):
    response = '''[
        {
            "title": "test title",
            "start_time": "start",
            "end_time": "end",
            "time": "1"
        },
        {
            "title": "test title",
            "start_time": "start",
            "end_time": "end",
            "time": "2"
        }
    ]'''
    #return HttpResponse(simplejson.dumps(response),
    return HttpResponse(response, mimetype="application/json")
