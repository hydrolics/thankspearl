from events.models import Event
from django.contrib.auth.models import User

import datetime
from django.utils.timezone import utc
import warnings

EVENT = {
    'title':  'test',
    'description':  'test',
    'user':  User.objects.get(pk=1),
}

OCCURRENCE = {
    'start_time':  datetime.datetime(2012, 9, 3, 0).replace(tzinfo=utc),
    'end_time':  datetime.datetime(2012, 9, 4, 0).replace(tzinfo=utc),
    'rrule_frequency':  'HOURLY',
    'rrule_params':  {'count': 3},
}


def create_event(title=EVENT['title'], description=EVENT['description'],
                 user=EVENT['user']):
    return Event.objects.create(title=title, description=description,
                                user=user)


def create_occurrence(event=None, start_time=OCCURRENCE['start_time'],
                      end_time=OCCURRENCE['end_time'],
                      rrule_frequency=OCCURRENCE['rrule_frequency'],
                      rrule_params=OCCURRENCE['rrule_params']):

    if not event:
        warnings.warn('Warning, no event given, assuming pk=1')
        event = Event.objects.get(pk=1)

    event.add_occurrences(
        start_time=start_time,
        end_time=end_time,
        rrule_frequency=rrule_frequency,
        rrule_params=rrule_params
    )

    return event.occurrence_set.all()[0]
